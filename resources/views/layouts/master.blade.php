<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AEx Bolivia - 2018</title>

    <link rel="stylesheet" href="/css/@yield('page').css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
</head>
<body>
    {{-- Include GA Tag here --}}

    @section('header')
        <header data-target="header">
            <nav id="nav" class="main-menu">
                <div class="wrapper text-center">
                    <ul>
                        <li data-link="header"><a href="/" title="início">início</a></li>
                        <li data-link="conferencistas"><a href="/conferencistas" title="Conferencistas">Conferencistas</a></li>
                        <li data-link="programacion"><a href="/programacion" title="programación">programación</a></li>
                        <li data-link="entradas"><a href="/entradas" title="entradas">entradas</a></li>
                        <li data-link="ubicacion"><a href="/ubicacion" title="ubicación">ubicación</a></li>
                        <li data-link="dudas"><a href="/dudas" title="¿dudas?">¿dudas?</a></li>
                        <li><a href="/entradas" class="btn action" title="comprar ahora">comprar ahora</a></li>
                    </ul>
                </div>
            </nav>
            <section class="content-header">
                <div class="wrapper text-center">
                    <h1><img src="/images/logotipo_aex_pocket_bolivia.svg" alt="AEx Pocket Bolivia 2018"></h1>
                    <h2><img src="/images/main_title.svg" alt="Su mejor experiencia latam en retail y trade marketing"></h2>
                    <span class="anchor navigate"></span>
                </div>

                <section class="sobre">
                    <div class="wrapper">
                        <h2><img src="/images/titulo_sobre.svg" alt="26 de Julio de 2018 - Santa Cruz de La Sierra"></h2>
                        <p>
                            AEx Pocket representa una versión itinerante de la mayor experiencia en retail y trade marketing de Latinoamérica, el Agile Experience - evento que ocurre anualmente en Brasil.
                            La iniciativa es una realización de Involves, y tiene como propósito objetivo atraer a profesionales que actúen directamente con trade marketing o áreas relacionadas.
                        </p>
                        <div class="text-left">
                            <span class="btn link">Conozca el evento</span>
                            <span class="anchor"></span>
                        </div>
                    </div>
                </section>
            </section>
        </header>
    @show

    <section class="content">
        @yield('content')
    </section>

    @section('footer')
        <footer>
            <div class="wrapper text-center">
                <h2 class="text-left"><img src="/images/titulo_patrocinadores.svg" alt="Patrocinadores"></h2>
                <h3 class="mb-50"><img src="images/titulo_gold.svg" alt="Patrocinadores Gold"></h3>
                
                <a href="#" target="_blank"><img src="images/logo_patroc_gold_brasilCT.svg" alt="Grupo Brasil.CT" width="500" height="117"></a>    
            </div>

            <nav id="navFooter" class="main-menu">
                <div class="wrapper text-center">
                    <a href="/" class="logo-aex inline"><img src="images/logo_aex_white.svg" alt="Aex" width="148" height="70"></a>

                    <ul class="inline">
                        <li data-link="conferencistas"><a href="/conferencistas" title="Conferencistas">Conferencistas</a></li>
                        <li data-link="programacion"><a href="/programacion" title="programación">programación</a></li>
                        <li data-link="entradas"><a href="/entradas" title="entradas">entradas</a></li>
                        <li data-link="ubicacion"><a href="/ubicacion" title="ubicación">ubicación</a></li>
                        <li data-link="dudas"><a href="/dudas" title="¿dudas?">¿dudas?</a></li>
                    </ul>

                    <div class="logo-partners inline">
                        <span class="logo inline">
                            <span class="title block">Curadoria</span>
                            <a href="https://clubedotrade.com.br/" target="_blank"><img src="images/logo_clube_do_trade_white.svg" alt="Clube do Trade" width="82" height="70"></a>    
                        </span>
                    
                        <span class="logo inline">
                            <span class="title block">Uma iniciativa</span>
                            <a href="https://www.involves.com.br" target="_blank"><img src="images/logo_involves.svg" alt="Involves" width="120" height="30"></a>
                        </span>
                    </div>
                    
                </div>
            </nav>
        </footer>
    @show

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/history.js/1.8/native.history.min.js"></script>
    <script src="/js/bundle.js" async defer></script>
</body>
</html>