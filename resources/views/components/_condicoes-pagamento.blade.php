<aside class="modal" data-visible="false">
    <h3>Quais as formas e condições de pagamento?</h3>
    <span class="fechar" title="fechar"></span>
    <ul class="lista">
        <li>
            <strong class="cartao">Cartão de crédito</strong>
            <p>Pagamento feito em até 3x sem juros nos cartões Mastercard, Visa, Amex e Dinners.</p>
        </li>
        <li>
            <strong class="dinheiro">Dinheiro</strong>
            <p>Será feita uma reserva de 3 dias do(s) ingresso(s). O pagamento deverá ser efetuado pessoalmente no escritório.</p>
        </li>
        <li>
            <strong class="cheque">Cheque</strong>
            <p>Será feita uma reserva de 3 dias do(s) ingresso(s). O pagamento deverá ser efetuado pessoalmente no escritório.</p>
        </li>
        <li>
            <strong class="transfer">Transferência bancária</strong>
            <p>Será feita uma reserva de 3 dias do(s) ingresso(s). O comprovante bancário deverá ser enviado para validação.</p>
        </li>
    </ul>
</aside>