<h3 class="subtitle" title="VIP Antecipado">
    <span class="text">Asientos preferenciales numerados en el evento, almuerzo con los conferencistas internacionales, primicias del material visual digital del evento y descuento en Marriott. Servicio de Valet Parking gratuito.</span>

    <div class="action-entradas">
        <span class="btn action">comprar ahora</span>
        <span class="formas-pagamento">
            Consulte as formas de pagamento
        </span>
    </div>

</h3>
<aside class="entrada">
    <div class="quantidade">
        <span class="counter">
            <span class="event less" data-type="remove"></span>
            <input class="number" value="1" required />
            <span class="event plus" data-type="add"></span>
        </span>
        <span class="valor">
            <strong>Bs <span class="field-bs">2100</span></strong>
            <span class="dollar">
                - US$<span class="field-usd">300</span>
            </span>
        </span>
    </div>
</aside>