<ul>
  <li class="speaker">
    <span class="name">Edgar Ureña</span>
    <span class="role">Colombia en Nielsen</span>
    <span class="photo"><img src="/images/photo_speaker.png" alt=""></span>

    <span class="more-info">
      <img src="/images/ico_pixel_info.svg" alt="Saiba mais sobre Edgar Ureña" width="30" height="30">
    </span>
  </li>
  <li class="speaker">
    <span class="name">Jorge Castrillon</span>
    <span class="role">Be Human</span>
    <span class="photo">
      <img src="/images/photo_speaker.png" alt="">
    </span>

    <span class="more-info">
      <img src="/images/ico_pixel_info.svg" alt="Saiba mais sobre Jorge Castrillon" width="30" height="30">
    </span>
  </li>
  <li class="speaker">
    <span class="name">Leonardo Zuppiroli</span>
    <span class="role">Playstation</span>
    <span class="photo">
      <img src="/images/photo_speaker.png" alt="">
    </span>

    <span class="more-info">
      <img src="/images/ico_pixel_info.svg" alt="Saiba mais sobre Leonardo Zuppiroli" width="30" height="30">
    </span>
  </li>
  <li class="speaker">
    <span class="name">Juan Manuel Ángel</span>
    <span class="role">Kraft Latam</span>
    <span class="photo">
      <img src="/images/photo_speaker.png" alt="">
    </span>

    <span class="more-info">
      <img src="/images/ico_pixel_info.svg" alt="Saiba mais sobre Juan Manuel Ángel" width="30" height="30">
    </span>
  </li>
  <li class="speaker">
    <span class="name">César Duro</span>
    <span class="role">Trade2Business</span>
    <span class="photo">
      <img src="/images/photo_speaker.png" alt="">
    </span>

    <span class="more-info">
      <img src="/images/ico_pixel_info.svg" alt="Saiba mais sobre César Duro" width="30" height="30">
    </span>
  </li>
</ul>

<!-- TEMPLATE PROFILE -->
<div class="profile-speaker hide">
  <span class="fechar" title="fechar"></span>

  <div class="main-infos inline text-center">
    <span class="name">Edgar Ureña</span>
    <a href="#" target="_blank" class="linkedin"><img src="images/ico_linkedin.svg" alt="Linkedin" width="31" height="31"></a>  
    <span class="role">Colombia en Nielsen</span>

    <span class="photo">
      <img src="/images/photo_speaker.png" alt="">
    </span>
  </div>
  
  <div class="general-infos inline">
    <p>Edgar Urueña tiene 8 años de experiencia en Nielsen, en el área de Analytics, como consultor para la industria de consumo masivo, ha trabajado con varias empresas en Colombia, Venezuela, Puerto Rico, CentroAmérica y República Dominicana. Su experiencia incluye el entendimiento de los retornos a la inversión en Marketing ayudando a sus clientes a establecer un plan y estrategia ganadora para lograr sus objetivos. Actualmente dirige las áreas de Marketing Mix y Pricing para Colombia para Nielsen Colombia.</p>

    <div class="lecture inline">
      <span class="title">PALESTRAS</span> 
      <span class="time">9:15</span>

      <div class="box">
        <small class="block"><i>TÍTULO</i></small>
        <i>Decisiones de precio estratégicas.</i>  
      </div>
    </div>

    <div class="share-speaker inline">
      <span class="title">Compartilhar palestrante:</span>

      <a href="#" class="social"><img src="images/ico_facebook.svg" alt="Facebook" width="60" height="60"></a>
      <a href="#" class="social"><img src="images/ico_linkedin.svg" alt="Linkedin" width="60" height="60"></a>
      <a href="#" class="social"><img src="images/ico_google.svg" alt="Google+" width="60" height="60"></a>
      <a href="#" class="social"><img src="images/ico_twitter.svg" alt="Twitter" width="60" height="60"></a>
    </div>

  </div>
</div>