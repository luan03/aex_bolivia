<dl>
  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Onde o evento será realizado?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Quais são as opções de pagamento?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Existe algum desconto para compra em grupo?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> O que está incluso na minha inscrição?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Posso comprar ingresso para um dia de evento?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Haverá transporte disponível?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> O que está incluso na minha inscrição?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> A hospedagem no Costão do Santinho está incluída no ingresso?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Vocês emitem certificado online?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Vocês emitem nota fiscal?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Incide algum imposto na minha compra? Qual?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Como funcionará o credenciamento no evento?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Não recebeu o seu e-mail de confirmação?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Posso transferir minha inscrição?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>

  <div class="container-duda">
    <dt class="question"><span class="ico-plus"></span> Como faço para patrocinar o AEx 2018?</dt>
    <dd class="answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor id tellus tincidunt ultricies. Sed fringilla porta augue ut vestibulum. Donec iaculis elementum nunc quis cursus. Ut mattis urna mauris, eleifend tempor sapien rhoncus eget.</dd>
  </div>
</dl>