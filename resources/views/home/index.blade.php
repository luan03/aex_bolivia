@extends('layouts.master')
@section('page', 'home')

@section('header')
    @parent
@endsection

@include('components/_condicoes-pagamento')

@section('content')

    <section class="conferencistas" data-target="conferencistas">
        <div class="wrapper">
            <h2><img src="/images/titulo_conferencistas.svg" alt="Conferencistas"></h2>
            @include('components/_conferencistas')
        </div>
    </section>

    <section class="programacion" data-target="programacion">
        <div class="wrapper">
            <h2><img src="/images/titulo_programacion.svg" alt="Programacion"></h2>
        </div>
    </section>

    <section class="entradas" data-target="entradas">
        <div class="wrapper">
            <h2><img src="/images/titulo_entradas.svg" alt="Entradas"></h2>
            @include('components/_entradas')
        </div>
    </section>

    <section class="ubicacion" data-target="ubicacion">
        <div class="wrapper">
            <h2><img src="/images/titulo_ubicacion.svg" alt="Ubicacion"></h2>
        </div>
    </section>

    <section class="dudas" data-target="dudas">
        <div class="wrapper">
            <h2><img src="/images/titulo_dudas.svg" alt="Dudas"></h2>
            @include('components/_dudas')
        </div>
    </section>
@endsection

@section('footer')
    @parent
@endsection