<?php

namespace App\Http\Controllers;

use App\Home;

class HomeController extends Controller
{
    /**
     * Return the view page
     *
     * @return Response
     */
    public function view()
    {
        return view('home.index');
    }
}