## Documentation AEx Bolivia 2018

### Back-End
Make sure that you have at least PHP 7.1.7 (cli) installed in your machine.
Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).

### Front-End
After clone this projet you have to follow some steps in order to run properly.

### Running locally
All front-end dependencies are managed through npm, so make sure that you have:

- Nodejs > 6.0
- NPM package manager, latest versions

### Project dependencies
1. Firstly, you have to update the dependencies `npm install`
2. Secondly, inside the main directory run on terminal `php -S localhost:8000 -t public`
3. Then `composer install` to install the project dependencies.
4. Finally, to watch changes on Less files run `gulp` in another terminal.

### Front-End project structure
```
public/
├── css/
│   ├── pageName.css - compiled and minified css files
│   |
resources/
├── less/
│   ├── layout/ - layout components
│   ├── pages/ - page components
├── views/
│   ├── pageName/ - HTML view pages
```

### Tests and validation

#### Desktop Browsers:

- IE 10+
- Chrome
- Firefox
- Safari

#### Mobile devices screen resolutions:

- Galaxy Note 3
- LG Optimus L70
- Nexus 5X
- Iphone 5
- Iphone 7
- Iphone 7 Plus
- iPad mini
- iPad
- iPad Pro

### Performance Budget

1. TTFB: máx. 600ms (Back-end)
2. Requests: máx. 50 (Front-End, not including third party resources such as analytics and tag managers)
3. Time Total Load: máx. 8 sec (Front-End and Back-End resources)
4. Keep alive: on (Server configuration)
5. Gzip: on (Server configuration)
6. Minify JS: on (Front-End)
7. Minify HTML: on (Back-End Laravel)
8. Minify CSS: on (Front-End)
9. Cache img, JS, CSS, SVG: on (Server configuration)


### Build

Before to build the application run `gulp build` on terminal then, commit and publish.

### Live

