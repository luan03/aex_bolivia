//calculate value entradas
function numeroEntradas() {
    $('.counter .event').on('click', function() {
        var type = $(this).data('type');
        var field = $('.number').val();

        if (field === '') {
            field = 0;
        }

        if (type === 'add') {
            field = parseInt(field) + 1;
        } else {
            if (field > 1) {
                field = parseInt(field) - 1;
            }
        }

        $('.number').val(field)

        calculatePurchase($('.number').val())
    })
}
//calculate value entradas
function actionFieldNumber() {
    $(".number").on("change paste keyup focusout", function() {

        if ($(this).val() === '' || $(this).val() == 0) {
            $(this).val(1) //TODOFIX: when update value
        }

        calculatePurchase($(this).val());
    });
}
//calculate value entradas
function calculatePurchase(number) {
    number = parseInt(number)

    var BS = 2100;
    var USD = 300;

    var valueBS = BS * number;
    var valueUSD = USD * number;

    $('.field-bs').text(valueBS);
    $('.field-usd').text(valueUSD);

}

function modalCondicoes() {
    $('.formas-pagamento').on('click', function(){

        var MARGIN_TOP = 6;
        var position = $(this).offset().top - MARGIN_TOP;

        $('.modal').attr('data-visible', true);
        $('.modal').css('top', position + 'px');
    })

    $('.fechar').on('click', function() {
        $('.modal').attr('data-visible', false);
    })
}

function scroll() {
    var text = window.location.pathname.replace('/', '');
    var HEADER_SIZE = 80;

    if (text === '') {
        text = 'header';
    }

    //TODO: fix wrong url

    $('html, body').stop( true, false ).animate({
        scrollTop: $('[data-target='+ text +']').offset().top - HEADER_SIZE
    }, 2000);

    $('#nav li').removeClass('active');
    $('#nav li[data-link='+ text +']').addClass('active');
}

function urlState() {
    $(".main-menu a").click(function() {
        History.pushState(null, document.title, $(this).attr('href'));

        scroll()
        return false;
    });
}

function OpenQuestion() {
    var question = document.getElementsByClassName("question");
    var i;

    for (i = 0; i < question.length; i++) {
        question[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var answer = this.nextElementSibling;

            if (answer.style.display === "block") {
                answer.style.display = "none";
                answer.className += ' ' + ico-plus;
            } else {
                answer.style.display = "block";
            }
        });
    }
}

$(window).on('load', function(){
    scroll()
    modalCondicoes();
    urlState();
    numeroEntradas();
    actionFieldNumber();
    OpenQuestion();
})

var Home = {

    scrollpos: window.scrollY,
    header: document.getElementById("nav"),

    addClass: function() {
        this.header.classList.add("fixed")
    },

    removeClass: function() {
        this.header.classList.remove("fixed")
    },

    scroll: function() {
        this.scrollpos = window.scrollY

        if (this.scrollpos > 93) {
            this.addClass()
        }
        else {
            this.removeClass()
        }
    }
};

window.addEventListener('scroll', function(){
    Home.scroll()
});
