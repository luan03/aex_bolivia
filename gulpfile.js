var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-csso');

var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

var pump = require('pump');
var runSequence = require('run-sequence');

const imagemin = require('gulp-imagemin');

// js task
gulp.task('scripts', function() {
    return gulp.src('resources/js/**/*.js')
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('public/js'))
});

// css task
gulp.task('css', function(){
  return gulp.src('resources/less/pages/*.less')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(gulp.dest('public/css'))
});

// compress js task
var jsFiles = 'resources/js/**/*.js',
    jsDest = 'public/js';

gulp.task('compressJs', function() {
    return gulp.src(jsFiles)
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});

// png compress
gulp.task('compressPNG', function(){
    return gulp.src(['public/images/**/*.png'])
        .pipe(imagemin({optimizationLevel: 5}))
        .pipe(gulp.dest('public/images'))
});

// works
gulp.task('watch', function() {
    gulp.watch('resources/less/**/*.less', ['css']);
    gulp.watch('resources/js/**/*.js', ['scripts']);
});

gulp.task('build', function(callback) {
    runSequence(['compressJs', 'compressPNG'])
});

gulp.task('default', ['watch']);